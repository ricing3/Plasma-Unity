# Plasma-Unity Rice



## Requeriments

**KDE Plasma** (duh)

**Latte Dock** (Compiled from source)
https://github.com/KDE/latte-dock

**Window Appmenu** (Compiled from source)
https://github.com/psifidotos/applet-window-appmenu

**Window Buttons** (Compiled from source)
https://github.com/psifidotos/applet-window-buttons

**Window Title** (Compiled from source)
https://github.com/psifidotos/applet-window-title

**Better Inline Clock applet** (KDE Store)

**Minimal Menu applet** (KDE Store)

**LiOS V cursors** (KDE Store)

**Tela Circle icons** (KDE Store)

**Esmoquin color scheme** (Download from this repo)

**Hide Titles KWin script** (KDE Store)

**Kvantum** (Distro Repos)

**Kvantum Style**: EsmoquinCrystal (Download from this repo)


## Configurations:

**Application Style**: Choose kvantum after installing and configuring it

**Plasma Style**: Breeze (The one that follows color scheme)

**Window Decorations**: Breeze (No borders, close and minimize to the left, title on the center)

**Fonts**: Liberation Sans 10pt for everything minus Fixed Width (Liberation Mono 10pt) and Small (Liberation Sans 8pt)

I don't use a splash screen, I like to boot quickly after the luks screen, but I did activate the KDE bootsound (System Settings > Notifications > Applications: Configure > Plasma Workspace > Configure Events > Login )

**Desktop Effects**: Background contrast, Blur (100% strenght, 33% noise), Desaturate Unresponsive Applications, Fading Popups, Full Screen, Login, Logout, Maximize, Morphing Popups, Sheet, Sliding Popups, Magic Lamp, Dialog Parent, Slide Back, Window Aperture, Slide, Overview, Scale

**Wallpaper**: Centered with Blur

**Desktop Layout**: Desktop

## License

**MIT+Nigger** <:^^^^)
